package presentation;

import java.awt.FlowLayout;
import java.awt.List;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.text.html.HTMLDocument.Iterator;

import com.mysql.jdbc.Field;

import model.Client;

public class Interfata extends JFrame{
	public Client client;
	private JButton buton1 = new JButton("Show All");
	private JButton buton2 = new JButton("Insert");
	private JButton buton3 = new JButton("Delete");
	private JButton buton4 = new JButton("Update");
	
	private JButton buton11 = new JButton("Show All");
	private JButton buton12 = new JButton("Insert");
	private JButton buton13 = new JButton("Delete");
	private JButton buton14 = new JButton("Update");
	
	private JButton buton111 = new JButton("Show All");
	private JButton buton112 = new JButton("Insert");
	private JButton buton113 = new JButton("Delete");
	private JButton buton114 = new JButton("Update");
	
	// Clienti
	private JLabel idu0 = new JLabel("IdUpdate");
	private JLabel idC = new JLabel("idClient");
	private JLabel numeC = new JLabel("Nume");
	private JLabel adresa = new JLabel("Adresa");
	private JLabel email = new JLabel("Email");
	private JLabel varsta = new JLabel("Varsta");
	
	private JTextField idU0=new JTextField(30);
	private JTextField idc = new JTextField(30);
	private JTextField numec = new JTextField(30);
	private JTextField adresac = new JTextField(30);
	private JTextField emailc = new JTextField(30);
	private JTextField varstac = new JTextField(30);
	
	//Produs
	private JLabel idu1=new JLabel("IdUpdate");
	private JLabel idP= new JLabel("idProdus");
	private JLabel numeP= new JLabel("Nume");
	private JLabel datafabP= new JLabel("DataFab");
	private JLabel dataexpP= new JLabel("DataExp");
	private JLabel producatorP= new JLabel("Producator");
	private JLabel cantitateP= new JLabel("Cantitate");
	private JLabel pretP= new JLabel("Pret");
	private JLabel unitateP= new JLabel("Unitate");
	
	private JTextField idU1=new JTextField(30);
	private JTextField idp= new JTextField(30);
	private JTextField numep= new JTextField(30);
	private JTextField datafabp= new JTextField(30);
	private JTextField dataexpp= new JTextField(30);
	private JTextField producatorp= new JTextField(30);
	private JTextField cantitatep= new JTextField(30);
	private JTextField pretp= new JTextField(30);
	private JTextField unitatep= new JTextField(30);
	
	//Bonuri
	private JLabel idu2=new JLabel("idUpdate");
	private JLabel id1 = new JLabel("idBonuri");
	private JLabel id2 = new JLabel("idClient");
	private JLabel id3 = new JLabel("idProdus");
	private JLabel nc = new JLabel("NumeClient");
	private JLabel np = new JLabel("NumeProdus");
	private JLabel cn = new JLabel("Cantitate");
	private JLabel pt = new JLabel("PretTotal");
	
	private JTextField idU2=new JTextField(30);
	private JTextField id11 = new JTextField(30);
	private JTextField id22 = new JTextField(30);
	private JTextField id33 = new JTextField(30);
	private JTextField nC = new JTextField(30);
	private JTextField nP = new JTextField(30);
	private JTextField cN = new JTextField(30);
	private JTextField pT= new JTextField(30);
	
	public Interfata() {
		 JFrame Clienti = new JFrame("Clienti");
		 JFrame Produse = new JFrame("Produse");
		 JFrame Bonuri = new JFrame("Bonuri");
		 
		 
		 JTable table2 = new JTable();
		 JTable table3 = new JTable();
		 
		 Clienti.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 Clienti.setSize(500,500);
		 
		 Produse.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 Produse.setSize(500,500);
		 
		 Bonuri.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 Bonuri.setSize(450,450);
		 //Clienti
		 JPanel c1 = new JPanel();
		 JPanel c2 = new JPanel();
		 JPanel c3 = new JPanel();
		 JPanel c4 = new JPanel();
		 JPanel c5 = new JPanel();
		 JPanel cu = new JPanel();
		 
		 cu.add(idu0);
		 cu.add(idU0);
		 cu.setLayout(new FlowLayout());
		 
		 c1.add(idC);
		 c1.add(idc);
		 c1.setLayout(new FlowLayout());
		 
		 c2.add(numeC);
		 c2.add(numec);
		 c2.setLayout(new FlowLayout());
		 
		 c3.add(adresa);
		 c3.add(adresac);
		 c3.setLayout(new FlowLayout());
		 
		 c4.add(email);
		 c4.add(emailc);
		 c4.setLayout(new FlowLayout());
		 
		 c5.add(varsta);
		 c5.add(varstac);
		 c5.setLayout(new FlowLayout());
		 
		 JPanel cc = new JPanel();
		 cc.add(c1);
		 cc.add(c2);
		 cc.add(c3);
		 cc.add(c4);
		 cc.add(c5);
		 cc.add(cu);
		 
		 JPanel cc1 = new JPanel();
		 cc1.add(buton1);
		 cc1.add(buton2);
		 cc1.add(buton3);
		 cc1.add(buton4);
		 cc1.setLayout(new BoxLayout(cc1,BoxLayout.X_AXIS));
		 cc.add(cc1);
		 cc.setLayout(new BoxLayout(cc,BoxLayout.Y_AXIS));
		 cc.setVisible(true);
		 
		 //Produse
		 
		 JPanel c6 = new JPanel();
		 JPanel c7 = new JPanel();
		 JPanel c8 = new JPanel();
		 JPanel c9 = new JPanel();
		 JPanel c10 = new JPanel();
		 JPanel c11 = new JPanel();
		 JPanel c12 = new JPanel();
		 JPanel c13 = new JPanel();
		 JPanel cu1 = new JPanel();
		 
		 cu1.add(idu1);
		 cu1.add(idU1);
		 cu1.setLayout(new FlowLayout());

		 c6.add(idP);
		 c6.add(idp);
		 c6.setLayout(new FlowLayout());
		 
		 c7.add(numeP);
		 c7.add(numep);
		 c7.setLayout(new FlowLayout());
		 
		 c8.add(datafabP);
		 c8.add(datafabp);
		 c8.setLayout(new FlowLayout());
		 
		 c9.add(dataexpP);
		 c9.add(dataexpp);
		 c9.setLayout(new FlowLayout());
		 
		 c10.add(producatorP);
		 c10.add(producatorp);
		 c10.setLayout(new FlowLayout());
		 
		 c11.add(cantitateP);
		 c11.add(cantitatep);
		 c11.setLayout(new FlowLayout());
		 
		 c12.add(pretP);
		 c12.add(pretp);
		 c12.setLayout(new FlowLayout());
		 
		 c13.add(unitateP);
		 c13.add(unitatep);
		 c13.setLayout(new FlowLayout());
		 
		 JPanel cp =new JPanel();
		 cp.add(c6);
		 cp.add(c7);
		 cp.add(c8);
		 cp.add(c9);
		 cp.add(c10);
		 cp.add(c11);
		 cp.add(c12);
		 cp.add(c13);
		 cp.add(cu1);
		 JPanel cp1 =new JPanel();
		
		 cp1.add(buton11);
		 cp1.add(buton12);
		 cp1.add(buton13);
		 cp1.add(buton14);
		 
		 
		 cp1.setLayout(new BoxLayout(cp1,BoxLayout.X_AXIS));
		 cp.add(cp1);
		// cp.add(cp2);
		 cp.setLayout(new BoxLayout(cp,BoxLayout.Y_AXIS));
		 cp.setVisible(true);
		 
		 //Bonuri
		 JPanel c14 = new JPanel();
		 JPanel c15 = new JPanel();
		 JPanel c16 = new JPanel();
		 JPanel c17 = new JPanel();
		 JPanel c18 = new JPanel();
		 JPanel c19 = new JPanel();
		 JPanel c20 = new JPanel();
		 JPanel cu2 = new JPanel();
		 
		 cu2.add(idu2);
		 cu2.add(idU2);
		 cu2.setLayout(new FlowLayout());
		 
		 c14.add(id1);
		 c14.add(id11);
		 c14.setLayout(new FlowLayout());
		 
		 c15.add(id2);
		 c15.add(id22);
		 c15.setLayout(new FlowLayout());
		 
		 c16.add(id3);
		 c16.add(id33);
		 c16.setLayout(new FlowLayout());
		 
		 c17.add(nc);
		 c17.add(nC);
		 c17.setLayout(new FlowLayout());
		 
		 c18.add(np);
		 c18.add(nP);
		 c18.setLayout(new FlowLayout());
		 
		 c19.add(cn);
		 c19.add(cN);
		 c19.setLayout(new FlowLayout());
		 
		 c20.add(pt);
		 c20.add(pT);
		 c20.setLayout(new FlowLayout());
		 
		 JPanel cb=new JPanel();
		 cb.add(c14);
		 cb.add(c15);
		 cb.add(c16);
		 cb.add(c17);
		 cb.add(c18);
		 cb.add(c19);
		 cb.add(c20);
		 cb.add(cu2);
		 
		 JPanel cb1=new JPanel();
		 cb1.add(buton111);
		 cb1.add(buton112);
		 cb1.add(buton113); 
		 cb1.add(buton114);
		 cb1.setLayout(new BoxLayout(cb1,BoxLayout.X_AXIS));	
		 cb.add(cb1);
		 cb.setLayout(new BoxLayout(cb,BoxLayout.Y_AXIS));
		 cb.setVisible(true);
		 
		 Clienti.add(cc);
		 
		 Clienti.setVisible(true);
		 
		 Produse.add(cp);
		
		 Produse.setVisible(true);
		 
		 Bonuri.add(cb);
		
		 Bonuri.setVisible(true);
	}
	
	public void butonShowAllC(ActionListener a) {
		buton1.addActionListener(a);
	}
	
	public void butonShowAllP(ActionListener a) {
		buton11.addActionListener(a);
	}
	
	public void butonShowAllB(ActionListener a) {
		buton111.addActionListener(a);
	}
	
	public void butonInsertC(ActionListener a) {
		buton2.addActionListener(a);
	}
	public void butonInsertP(ActionListener a) {
		buton12.addActionListener(a);
	}
	public void butonInsertB(ActionListener a) {
		buton112.addActionListener(a);
	}
	
	public void butonDeleteC(ActionListener a) {
		buton3.addActionListener(a);
	}
	public void butonDeleteP(ActionListener a) {
		buton13.addActionListener(a);
	}
	public void butonDeleteB(ActionListener a) {
		buton113.addActionListener(a);
	}
	
	public void butonUpdateC(ActionListener a) {
		buton4.addActionListener(a);
	}
	public void butonUpdateP(ActionListener a) {
		buton14.addActionListener(a);
	}
	public void butonUpdateB(ActionListener a) {
		buton114.addActionListener(a);
	}
	
	//Clienti
	public String get1() {
		return idc.getText();
	}
	public String get2() {
		return numec.getText();
	}
	public String get3() {
		return adresac.getText();
	}
	public String get4() {
		return emailc.getText();
	}
	public String get5() {
		return varstac.getText();
	}
	
	//Produse
	public String get6() {
		return idp.getText();
	}
	public String get7() {
		return numep.getText();
	}
	public String get8() {
		return datafabp.getText();
	}
	public String get9() {
		return dataexpp.getText();
	}
	public String get10() {
		return producatorp.getText();
	}
	public String get11() {
		return cantitatep.getText();
	}
	public String get12() {
		return pretp.getText();
	}
	public String get13() {
		return unitatep.getText();
	}
	
	//Bonuri
	public String get14() {
		return id11.getText();
	}
	public String get15() {
		return id22.getText();
	}
	public String get16() {
		return id33.getText();
	}
	public String get17() {
		return nC.getText();
	}
	public String get18() {
		return nP.getText();
	}
	public String get19() {
		return cN.getText();
	}
	public String get20() {
		return pT.getText();
	}
	
	public String getu0() {
		return idU0.getText();
	}
	
	public String getu1() {
		return idU1.getText();
	}
	
	public String getu2() {
		return idU2.getText();
	}
	
	public void showException(String mesaj) {
		JOptionPane.showInputDialog(client, mesaj);
	}
	
	public <T> void createTable(ArrayList<T> list) {
		Class clasa = list.getClass();							// returneaza un obiect asociat clase resp
		java.util.Iterator<T> it = list.iterator();				// iterator pentru a parcurge lista
		
		java.lang.reflect.Field[] f = null;  
		Object obj = null;
		Object[] values = null;									
		String[] columnsName = null;
		String[] rows = null;
		
		int i = 0;
		int j = 0;
		int ok = 0;
		if (list != null)
		f = list.get(0).getClass().getDeclaredFields(); 			// se returneaza variabilele de instanta
		
		String[][] data = new String[list.size()][f.length]; 		// matricea cu continutul din tabele
		columnsName = new String[f.length];    						//cap de tabel
		values = new Object[f.length];
		rows = new String[f.length];
		while (it.hasNext()) {
			Object temp = it.next();								// primeste un obiect de tipul dat
			for (java.lang.reflect.Field field : f) {  				// parcurgem fieldurile pe rand
				field.setAccessible(true);							// se pot vedea variabilele private	

					columnsName[i] = field.getName();				// populam antetul tabelului
					try {
						values[i] = field.get(temp);				// returneaza continutul 
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
					rows[i] = values[i].toString();					// din values duc in rows care vor fi introduse in tabela
					i++;
			 
			}

			for (int k = 0; k < f.length; k++) {					// aici populez matricea cu fiecare rand 
				data[j][k] = rows[k];
				
			}
			j++;
			i=0;
		}

		JFrame frame1 = new JFrame();
		JTable clientiTable = new JTable(data, columnsName);
		clientiTable.setCellSelectionEnabled(true);
		clientiTable.setBounds(300, 400, 200, 300);
		JScrollPane sp = new JScrollPane(clientiTable);
		frame1.add(sp);
		frame1.setSize(400, 500);
		frame1.setVisible(true);

	}
}
