package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;

import bll.BonuriBLL;
import bll.ClientiBLL;
import bll.ProdusBLL;
import model.Bonuri;
import model.Client;
import model.Produs;

public class Control {
	
	 private ArrayList<Client> clienti;
	 private ArrayList<Produs> produse;
	 private ArrayList<Bonuri> bonuri;
	 
	 private ClientiBLL c1=new ClientiBLL();
	 private ProdusBLL P = new ProdusBLL();
	 private BonuriBLL b1 = new BonuriBLL();
	 private Interfata interfata;
	 
	  public Control(Interfata interfata1){
		 interfata=interfata1;
		 
		 interfata.butonDeleteC(new DeleteCListener());
		 interfata.butonDeleteP(new DeletePListener());
		 interfata.butonDeleteB(new DeleteBListener());
		 interfata.butonShowAllC(new ShowAllCListener());
		 interfata.butonShowAllP(new ShowAllPListener());
		 interfata.butonShowAllB(new ShowAllBListener());
		 interfata.butonInsertC(new InsertCListener());
		 interfata.butonInsertP(new InsertPListener());
		 interfata.butonInsertB(new InsertBListener());
		 interfata.butonUpdateC(new UpdateCListener());
		 interfata.butonUpdateP(new UpdatePListener());
		 interfata.butonUpdateB(new UpdateBListener());
	 }
	 
	 class InsertCListener implements ActionListener{
		 public void actionPerformed(ActionEvent e) {
				
			 String idClient = interfata.get1();
			 String Nume = interfata.get2();
			 String Adresa = interfata.get3();
			 String Email = interfata.get4();
			 String varsta = interfata.get5();
			 int id= Integer.parseInt(idClient);
			 int v = Integer.parseInt(varsta);
			 
			 Client c = new Client(id,Nume,Adresa,Email,v);
			 
			 c1.insertClienti(c);
		 }
	 }
	 
	 class InsertPListener implements ActionListener{
		 public void actionPerformed(ActionEvent e) {
			 String idProdus = interfata.get6();
			 String numep = interfata.get7();
			 String datafab = interfata.get8();
			 String dataexp = interfata.get9();
			 String producator = interfata.get10();
			 String cantitate= interfata.get11();
			 String pret = interfata.get12();
			 String unitate = interfata.get13();
			 
			 int id = Integer.parseInt(idProdus);
			 int c= Integer.parseInt(cantitate);
			 int p1= Integer.parseInt(pret);
			 
			 Produs p = new Produs(id,numep,datafab,dataexp,producator,c,p1,unitate);
			 P.insertProduse(p);
		 }
	 }
	 
	 class InsertBListener implements ActionListener{
		 public void actionPerformed(ActionEvent e) {
			 String idBon = interfata.get14();
			 String idC = interfata.get15();
			 String idP = interfata.get16();
			 String numeC = interfata.get17();
			 String numeP = interfata.get18();
			 String cantitate= interfata.get19();
			 String pret = interfata.get20();
			 
			 int id1= Integer.parseInt(idBon);
			 int id2= Integer.parseInt(idC);
			 int id3= Integer.parseInt(idP);
			 int c= Integer.parseInt(cantitate);
			 int p= Integer.parseInt(pret);
			 
			 Bonuri b= new Bonuri(id1,id2,id3,numeC,numeP,c,p);
			 b1.insertBonuri(b);
			 try {
				 PrintWriter bon = new PrintWriter("Nota de plata.txt","UTF-8");
				 bon.println("Comanda:");
				 bon.println("Cu id-ul -> "+id1);
				 bon.println("Cu id-ul client -> "+id2);
				 bon.println("Cu id-ul produs -> "+id3);
				 bon.println("A clientului -> "+numeC);
				 bon.println("Care a cumparat produsul -> "+numeP);
				 bon.println("Cantitatea -> "+c);
				 bon.println("Pret final -> "+p+p*c);
				 bon.close();
			 }catch(Exception e1) {
				 System.out.println(e1.getMessage()+"la scriere in fisier");
			 }
		 }
	 }
	 
	 class DeleteCListener implements ActionListener{
		 public void actionPerformed(ActionEvent e) {
			 String id1 = interfata.get1();
			 int id= Integer.parseInt(id1);
			 
			 c1.deleteClientById(id);
		 }
	 }
	 
	 class DeletePListener implements ActionListener{
		 public void actionPerformed(ActionEvent e) {
			 String id1 = interfata.get6();
			 int id= Integer.parseInt(id1);
			 
			 P.deleteProdusById(id);
		 }
	 }
	 
	 class DeleteBListener implements ActionListener{
		 public void actionPerformed(ActionEvent e) {
			 String id1 = interfata.get14();
			 int id= Integer.parseInt(id1);
			 
			 b1.deleteBonuriById(id);
		 }
	 }
	
	 class ShowAllCListener implements ActionListener{
		 public void actionPerformed(ActionEvent e) {
			 
			 clienti = c1.FindAll();
			 /*String[] columns= {"idClient","Nume","Adresa","Email","Varsta"};
			 String data[][]= new String[50][5];
			 String row;
			 int indexMatrice=0;
			 for(Client t: clienti) {
			 
			 String[] rowElements = new String[5];
			 rowElements[0]=Integer.toString(t.getId());
			 rowElements[1]=new String(t.getNume());
			 rowElements[2]=new String(t.getAdresa());
			 rowElements[3]=new String(t.getEmail());
			 rowElements[4]=Integer.toString(t.getVarsta());
			 
			 data[indexMatrice]=rowElements;
			 indexMatrice++;
			 }
			
			 JTable table1 = new JTable(data,columns);
			 table1.setCellSelectionEnabled(true);
			 table1.setBounds(300,400,200,300);
			 JScrollPane sp = new JScrollPane(table1);
			 
			 JFrame f=new JFrame();
			 f.add(sp);
			 f.setSize(300,400);
			 f.setVisible(true);
			 */
			 
			 interfata.createTable(clienti);
		 }
	 }
	 
	 class ShowAllPListener implements ActionListener{
		 public void actionPerformed(ActionEvent e) {
			 
			 produse = P.FindAll();
			 interfata.createTable(produse);
			 /*(String[] columns= {"idProdus","Nume","DataFab","DataExp","Producator","Cantitate","Pret","Unitate"};
			 String data[][]= new String[50][8];
			 String row;
			 int indexMatrice=0;
			 for(Produs t: produse) {
			 
			 String[] rowElements = new String[8];
			 rowElements[0]=Integer.toString(t.getId());
			 rowElements[1]=new String(t.getNume());
			 rowElements[2]=new String(t.getDataFab());
			 rowElements[3]=new String(t.getDataExp());
			 rowElements[4]=new String(t.getProducator());
			 rowElements[5]=Integer.toString(t.getCant());
			 rowElements[6]=Integer.toString(t.getPret());
			 rowElements[7]=new String(t.getUnitate());
			 
			 data[indexMatrice]=rowElements;
			 indexMatrice++;
			 }
			
			 JTable table1 = new JTable(data,columns);
			 table1.setCellSelectionEnabled(true);
			 table1.setBounds(300,400,200,300);
			 JScrollPane sp = new JScrollPane(table1);
			 
			 JFrame f=new JFrame();
			 f.add(sp);
			 f.setSize(300,400);
			 f.setVisible(true);
			 
		 }*/
		 }	 
	 }
	 
	 class ShowAllBListener implements ActionListener{
		 public void actionPerformed(ActionEvent e) {
			 
			 bonuri = b1.findAll();
			 interfata.createTable(bonuri);
			 /* String[] columns= {"idBonuri","idClient","idProdus","NumeC","NumeP","Cantitate","Pret final"};
			 String data[][]= new String[50][7];
			
			 int indexMatrice=0;
			 for(Bonuri t: bonuri) {
			 
			 String[] rowElements = new String[7];
			 rowElements[0]=Integer.toString(t.getIdBonuri());
			 rowElements[1]=Integer.toString(t.getIdClient());
			 rowElements[2]=Integer.toString(t.getIdProdus());
			 rowElements[3]=new String(t.getNumeC());
			 rowElements[4]=new String(t.getNumeP());
			 rowElements[5]=Integer.toString(t.getCantitate());
			 rowElements[6]=Integer.toString(t.getPretfinal());
			 
			 data[indexMatrice]=rowElements;
			 indexMatrice++;
			 }
			
			 JTable table1 = new JTable(data,columns);
			 table1.setCellSelectionEnabled(true);
			 table1.setBounds(300,400,200,300);
			 JScrollPane sp = new JScrollPane(table1);
			 
			 JFrame f=new JFrame();
			 f.add(sp);
			 f.setSize(300,400);
			 f.setVisible(true);
			 
		 }*/
	 }
	}
	 class UpdateCListener implements ActionListener{
		 public void actionPerformed(ActionEvent e) {
			 String idUpdate = interfata.getu0();
			 String idClient = interfata.get1();
			 String Nume = interfata.get2();
			 String Adresa = interfata.get3();
			 String Email = interfata.get4();
			 String varsta = interfata.get5();
			 int id= Integer.parseInt(idClient);
			 int v = Integer.parseInt(varsta);
			 int idu= Integer.parseInt(idUpdate);
			 
			 Client c = new Client(id,Nume,Adresa,Email,v);
			 
			 try {
				c1.updateClienti(c,idu);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		 }
	 }
	 
	 class UpdatePListener implements ActionListener{
		 public void actionPerformed(ActionEvent e) {
			 String idUpdate = interfata.getu1();
			 
			 String idProdus = interfata.get6();
			 String Nume = interfata.get7();
			 String DataFab= interfata.get8();
			 String DataExp = interfata.get9();
			 String Producator = interfata.get10();
			 String cant = interfata.get11();
			 String pret = interfata.get12();
			 String unit = interfata.get13();
			 
			 int idu= Integer.parseInt(idUpdate);
			 int id = Integer.parseInt(idProdus);
			 int cantitate= Integer.parseInt(cant);
			 int pret1= Integer.parseInt(pret);
			 
			 Produs p = new Produs(id,Nume,DataFab,DataExp,Producator,cantitate,pret1,unit);
			 
			 try {
				P.updateProdus(p,idu);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		 }
	 }
	 
	 class UpdateBListener implements ActionListener{
		 public void actionPerformed(ActionEvent e) {
			 String idUpdate = interfata.getu2();
			 
			 String idBonuri = interfata.get14();
			 String idClient = interfata.get15();
			 String idProdus = interfata.get16();
			 String nume = interfata.get17();
			 String produs = interfata.get18();
			 String cant = interfata.get19();
			 String pf = interfata.get20();
			 
			 int idc= Integer.parseInt(idClient);
			 int idp = Integer.parseInt(idProdus);
			 int idb = Integer.parseInt(idBonuri);
			 int idu= Integer.parseInt(idUpdate);
			 int c= Integer.parseInt(cant);
			 int p= Integer.parseInt(pf);
			 
			 Bonuri b = new Bonuri(idb,idc,idp,nume,produs,c,p);
			 
			 try {
				b1.updateBonuri(idu, b);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		 }
	 }
	}
