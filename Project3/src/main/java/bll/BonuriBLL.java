package bll;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import bll.validators.Validator;
import dao.BonuriDAO;
import model.Bonuri;
import model.Produs;

public class BonuriBLL {

	private ArrayList<Validator<Bonuri>> validators;
	
	public BonuriBLL() {
		
		validators = new ArrayList<Validator<Bonuri>>();
		validators.add(null);
	}
	
	public Bonuri findBonuriByID(int id) {
		Bonuri st = BonuriDAO.findById(id);
		if(st==null) {
			throw new NoSuchElementException("No data with id "+ id +"exists");
		}
		return st;
	}
	
	public ArrayList<Bonuri> findAll() {
		ArrayList<Bonuri> st = BonuriDAO.findAll();
		if(st==null) {
			throw new NoSuchElementException("There aren't any data ");
		}
		return st;
	}
	
	public int insertBonuri (Bonuri bon) {
		
		return BonuriDAO.insert(bon);
	}
	
	public Bonuri deleteBonuriById(int id) {
		Bonuri st = BonuriDAO.deleteProdus(id);
		if(st != null) {
			throw new NoSuchElementException("No data with id "+ id +"exists");
		}
		return st;
	}
	
	public int updateBonuri(int id,Bonuri b) throws SQLException {
		return BonuriDAO.update(id, b);
	}
}
