package bll;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import javax.swing.JFrame;

import bll.validators.CantValidator;
import bll.validators.PretValidator;
import bll.validators.UnitValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import dao.ProdusDAO;
import model.Client;
import model.Produs;

public class ProdusBLL {

	private ArrayList<Validator<Produs>> validators;
	
	public ProdusBLL() {
		
		validators = new ArrayList<Validator<Produs>>();
		validators.add(new CantValidator());
		validators.add(new PretValidator());
		validators.add(new UnitValidator());
	}
	
	public ArrayList<Produs> FindAll(){
		ArrayList<Produs> st = ProdusDAO.findAll();
		if(st == null) {
			throw new NoSuchElementException("There aren't any in this database");
		}
		return st;
	}
	public Produs findProdusById(int id) {
		Produs st =ProdusDAO.findById(id);
		if(st== null) {
			throw new NoSuchElementException("The product with id="+ id + "doesnt exists");

			}
		return st;
		}
	
	public int insertProduse (Produs produs) {
		for ( Validator<Produs> v: validators) {
			v.validate(produs);
		}
		return ProdusDAO.insert(produs);
	}
	
	public Produs deleteProdusById(int id) {
		Produs st = ProdusDAO.deleteProdus(id);
		if(st != null) {
			throw new NoSuchElementException("The product with id="+ id + "doesnt exists");
		}
		return st;
	}
	
	public int updateProdus(Produs p,int id) throws SQLException {
		return ProdusDAO.update(id,p);
	}
}
