package bll;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import bll.validators.ClientAgeValidator;
import bll.validators.EmailValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import model.Client;

public class ClientiBLL {

	private ArrayList<Validator<Client>> validators;
	
	public ClientiBLL() {
		
		validators = new ArrayList<Validator<Client>>();
		validators.add(new EmailValidator());
		validators.add(new ClientAgeValidator());
		
	}
	
	public Client findClientById(int id) {
		Client st= ClientDAO.findById(id);
		if(st == null) {
			throw new NoSuchElementException("The client with id="+ id + "doesnt exists");
		}
		return st;
	}
	
	public ArrayList<Client> FindAll() {
		ArrayList<Client> st = ClientDAO.findAll();
		if(st==null) {
			throw new NoSuchElementException("There aren't any in this database");
		}
		return st;
	}
	
	public int insertClienti(Client client) {
		for(Validator<Client> v : validators) {
			v.validate(client);
		}
		return ClientDAO.insert(client);
	}
	
	public Client deleteClientById(int id) {
		Client st = ClientDAO.deleteClient(id);
		if(st != null) {
			throw new NoSuchElementException("The client with id="+ id + "doesnt exists");
		}
		return st;
	}
	
	public int updateClienti(Client client,int id) throws SQLException {
		return ClientDAO.update(id, client);
	}
}
