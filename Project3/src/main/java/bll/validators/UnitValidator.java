package bll.validators;

import model.Produs;

public class UnitValidator implements Validator <Produs>{
	
	public void validate(Produs p) {
		int nr=0;
			if(p.getUnitate().matches("l")) {
				nr++;
			}
			if(p.getUnitate().matches("L")) {
				nr++;
			}
			if(p.getUnitate().matches("kg")) {
				nr++;
			}
			if(p.getUnitate().matches("g")) {
				nr++;
			}
			if(p.getUnitate().matches("G")) {
				nr++;
			}
			if(p.getUnitate().matches("Kg")) {
				nr++;
			}
			if(p.getUnitate().matches("kG")) {
				nr++;
			}
			if(p.getUnitate().matches("KG")) {
				nr++;
			}
			if(nr==0) {
				throw new IllegalArgumentException("Ati introdus o unitate de masura gresita");
			}
	}
}