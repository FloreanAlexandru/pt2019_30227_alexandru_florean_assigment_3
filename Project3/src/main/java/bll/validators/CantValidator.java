package bll.validators;

import java.awt.FlowLayout;

import javax.swing.*;

import model.Produs;

public class CantValidator implements Validator <Produs>{
	
	public void validate(Produs p) {
		if(p.getCant() < 0) {
			throw new IllegalArgumentException("Nu putem avea cantitatea mai mica de 0");
			
			
		}
	}
}
