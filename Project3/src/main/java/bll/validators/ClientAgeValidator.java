package bll.validators;

import model.Client;

public class ClientAgeValidator implements Validator <Client> {
	
	public void validate(Client t) {
		// TODO Auto-generated method stub
		
		if(t.getVarsta() < 8 || t.getVarsta() > 100) {
			throw new IllegalArgumentException("Varsta clientului este prea mare sau prea mica");
		}
	}
}
