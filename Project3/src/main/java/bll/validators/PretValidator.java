package bll.validators;

import model.Produs;

public class PretValidator implements Validator <Produs>{
	
	public void validate(Produs p) {
		if(p.getPret() <= 0) {
			throw new IllegalArgumentException("Nu putem avea pretul mai mic de 0");
		}
	}
}
