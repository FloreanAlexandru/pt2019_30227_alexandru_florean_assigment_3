package model;

public class Bonuri {
	
	private int idBonuri;
	private int idClient;
	private int idProdus;
	private String NumeC;
	private String NumeP;
	private int cantitate;
	private int pretfinal;
	
	public Bonuri(int idBonuri, int idClient, int idProdus, String numeC, String numeP, int cantitate,int pfinal) {
		super();
		this.idBonuri = idBonuri;
		this.idClient = idClient;
		this.idProdus = idProdus;
		this.NumeC = numeC;
		this.NumeP = numeP;
		this.cantitate = cantitate;
		this.pretfinal= pfinal;
	}
	
	public Bonuri(int idClient, int idProdus, String numeC, String numeP, int cantitate, int pfinal) {
		super();
		this.idClient = idClient;
		this.idProdus = idProdus;
		this.NumeC = numeC;
		this.NumeP = numeP;
		this.cantitate = cantitate;
		this.pretfinal = pfinal;
	}

	public int getIdBonuri() {
		return idBonuri;
	}
	public void setIdBonuri(int idBonuri) {
		this.idBonuri = idBonuri;
	}
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	public int getIdProdus() {
		return idProdus;
	}
	public void setIdProdus(int idProdus) {
		this.idProdus = idProdus;
	}
	public String getNumeC() {
		return NumeC;
	}
	public void setNumeC(String numeC) {
		NumeC = numeC;
	}
	public String getNumeP() {
		return NumeP;
	}
	public void setNumeP(String numeP) {
		NumeP = numeP;
	}
	public int getCantitate() {
		return cantitate;
	}
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	public int getPretfinal() {
		return pretfinal;
	}

	public void setPretfinal(int pretfinal) {
		this.pretfinal = pretfinal;
	}

	@Override
	public String toString() {
		return "Bonuri [idBonuri=" + idBonuri + ", idClient=" + idClient + ", idProdus=" + idProdus + ", NumeC=" + NumeC
				+ ", NumeP=" + NumeP + ", cantitate=" + cantitate + ", pretfinal=" + pretfinal + "]";
	}
		
}
