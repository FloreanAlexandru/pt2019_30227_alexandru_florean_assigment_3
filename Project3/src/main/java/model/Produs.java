package model;

public class Produs {
	private int id;
	private String Nume;
	private String Producator;
	private String DataFab;
	private String DataExp;
	private int cant;
	private String Unitate;
	private int pret;
	public Produs(int id, String nume, String producator, String dataFab, String dataExp, int cant,int pret,String unitate) {
		super();
		this.id = id;
		this.Nume = nume;
		this.Producator = producator;
		this.DataFab = dataFab;
		this.DataExp = dataExp;
		this.cant = cant;
		this.pret=pret;
		this.Unitate =unitate;
	}
	
	public Produs(String nume, String producator, String dataFab, String dataExp, int cant,int pret,String unitate) {
		super();
		this.Nume = nume;
		this.Producator = producator;
		this.DataFab = dataFab;
		this.DataExp = dataExp;
		this.cant = cant;
		this.pret=pret;
		this.Unitate=unitate;
	}
	
	public int getCant() {
		return cant;
	}
	public void setCant(int cant) {
		this.cant = cant;
	}
	public String getUnitate() {
		return Unitate;
	}
	public void setUnitate(String unitate) {
		Unitate = unitate;
	}
	public int getPret() {
		return pret;
	}
	public void setPret(int pret) {
		this.pret = pret;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNume() {
		return Nume;
	}
	public void setNume(String nume) {
		Nume = nume;
	}
	public String getProducator() {
		return Producator;
	}
	public void setProducator(String producator) {
		Producator = producator;
	}
	public String getDataFab() {
		return DataFab;
	}
	public void setDataFab(String dataFab) {
		DataFab = dataFab;
	}
	public String getDataExp() {
		return DataExp;
	}
	public void setDataExp(String dataExp) {
		DataExp = dataExp;
	}

	@Override
	public String toString() {
		return "Produs [id=" + id + ", Nume=" + Nume + ", Producator=" + Producator + ", DataFab=" + DataFab
				+ ", DataExp=" + DataExp + ", cant=" + cant + ", Unitate=" + Unitate + ", pret=" + pret + "]";
	}
	
}
