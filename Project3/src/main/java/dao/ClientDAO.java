package dao;

import java.sql.Connection;
import Connection.ConnectionFactory;
import bll.ClientiBLL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mysql.fabric.xmlrpc.base.Array;
import com.mysql.jdbc.Statement;
import model.Client;

public class ClientDAO {

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO client (idClient,nume,adresa,email,varsta)" + " VALUES (?,?,?,?,?) ";
	private final static String deleteStatementString ="Delete from client where idClient =?";
	private final static String findStatementString= "Select * From client ";	
	private final static String updateStatementString= "Update client SET nume=?, adresa=?, email=?, varsta=? where idClient=?";
	private final static String findStatementString1 ="Select * From client where idClient =?";
	
	public static Client findById(int clientId) {
		Client returnez = null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs=null;
		
		try {
			findStatement = dbConnection.prepareStatement(findStatementString1);
			findStatement.setLong(1, clientId);
			rs = findStatement.executeQuery();
			rs.next();
			
			String nume = rs.getString("Nume");
			String adresa = rs.getString("Adresa");
			String email = rs.getString("Email");
			int varsta = rs.getInt("Varsta");
			returnez = new Client(clientId,nume,adresa,email,varsta);
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById "+ e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return returnez;
	}

	public static ArrayList<Client> findAll() {
		ArrayList<Client> returnez = new ArrayList<Client>();
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs=null;
		
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			
			rs = findStatement.executeQuery();
			while(rs.next()) {
			
			int idClient = rs.getInt("idClient");
			String nume = rs.getString("Nume");
			String adresa = rs.getString("Adresa");
			String email = rs.getString("Email");
			int varsta = rs.getInt("Varsta");
			returnez.add(new Client(idClient,nume,adresa,email,varsta));
			
			}	
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById "+ e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return returnez;
		
	}
	public static int insert(Client client) {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		
		PreparedStatement insertStatement = null;
		
		int insertedId = -1;
		
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, client.getId());
			insertStatement.setString(2, client.getNume());
			insertStatement.setString(3, client.getAdresa());
			insertStatement.setString(4, client.getEmail());
			insertStatement.setInt(5, client.getVarsta());
			insertStatement.executeUpdate();
			
			ResultSet rs = insertStatement.getGeneratedKeys();
			if(rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO insert "+ e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	
	public static Client deleteClient(int idClient) {
		Client returnez =null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		int rs = 0;
		
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setLong(1,idClient);
			rs = deleteStatement.executeUpdate();
						
		}catch(SQLException e){
			LOGGER.log(Level.WARNING,"ClientDAO delete "+ e.getMessage());
		}
		finally {
			
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		return returnez;
	}

	public static int update(int id,Client client) throws SQLException {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		PreparedStatement subStatement = null;
		
		int Id;
		String nume;
		String email;
		String adresa;
		int varsta;
		
		int insertedId = -1;
		
		try {

			insertStatement = dbConnection.prepareStatement(insertStatementString,Statement.RETURN_GENERATED_KEYS);
			ResultSet rs1= null;
			
			subStatement = dbConnection.prepareStatement(findStatementString1,Statement.RETURN_GENERATED_KEYS);
			subStatement.setLong(1, id);
			rs1 = subStatement.executeQuery();
			
			while(rs1.next()) {	
					Id = rs1.getInt("idClient");
					nume= rs1.getString("Nume");
					email = rs1.getString("Adresa");
					adresa = rs1.getString("Email");
					varsta = rs1.getInt("varsta");
				
			//Client C=new Client(Id,nume,email,adresa,varsta);
			ClientiBLL clientbll= new ClientiBLL();							
			clientbll.deleteClientById(id);
			clientbll.insertClienti(client);

		}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO update "+ e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(subStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	
	public static int update1(int id,Client client) throws SQLException {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		int rs1= 0;
		ClientiBLL client1 = new ClientiBLL();
		Client c=client1.findClientById(id);
		try {
			
			int Id=client.getId();
			String nume=client.getNume();
			String email=client.getEmail();
			String adresa=client.getAdresa();
			int varsta=client.getVarsta();
			
			System.out.println(c.getId()+" "+c.getAdresa()+" "+c.getNume()+" AICI");
			System.out.println(Id+" "+ nume + email + adresa + varsta +"AICI");
			
			
			updateStatement = dbConnection.prepareStatement(updateStatementString,Statement.RETURN_GENERATED_KEYS);
			updateStatement.setInt(1, Id);				
			updateStatement.setString(2, nume);
			updateStatement.setString(3, adresa);
			updateStatement.setString(4, email);
			updateStatement.setInt(5, varsta);

			rs1 = updateStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO update "+ e.getMessage());
		} finally {
			
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return rs1;
	}
}
