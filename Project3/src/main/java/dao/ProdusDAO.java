package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mysql.jdbc.Statement;

import Connection.ConnectionFactory;
import bll.ClientiBLL;
import bll.ProdusBLL;
import model.Client;
import model.Produs;

public class ProdusDAO {

	protected static final Logger LOGGER = Logger.getLogger(ProdusDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO produs (idProdus,nume,producator,datafab,dataexp,cant,pret,unitate)"+"VALUES(?,?,?,?,?,?,?,?)";
	private final static String findStatementString= "Select * From produs ";	
	private final static String findStatementString1="Select * From produs where idProdus= ?";
	private final static String deleteStatementString= "Delete From produs where idProdus = ?";
	private final static String updateStatementString= "Update produs SET nume=?, datafab=?, dataexp=?, producator=? cant=?, pret=?, unitate=? where idClient=?";

	public static Produs findById(int ProdusId) {
		Produs returnez = null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			findStatement = dbConnection.prepareStatement(findStatementString1);
			findStatement.setLong(1, ProdusId);
			rs = findStatement.executeQuery();
			rs.next();
			
			String nume = rs.getString("Nume");
			String producator = rs.getString("Producator");
			String datafab = rs.getString("DataFab");
			String dataexp = rs.getString("DataExp");
			int cant= rs.getInt("cant");
			String unitate= rs.getString("unitate");
			int pret = rs.getInt("pret");
			returnez = new Produs(ProdusId,nume,producator,datafab,dataexp,cant,pret,unitate);
			
			
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING,"ProdusDAO:findById "+ e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return returnez;
	}

	public static ArrayList<Produs> findAll() {
		ArrayList<Produs> returnez = new ArrayList<Produs>();
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			rs = findStatement.executeQuery();
			
			while(rs.next()) {
			int idProdus = rs.getInt("idProdus");
			String nume = rs.getString("Nume");
			String producator = rs.getString("Producator");
			String datafab = rs.getString("DataFab");
			String dataexp = rs.getString("DataExp");
			int cant= rs.getInt("Cant");
			int pret = rs.getInt("Pret");
			String unitate= rs.getString("Unitate");
			
			returnez.add(new Produs(idProdus,nume,producator,datafab,dataexp,cant,pret,unitate));
			}
			
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING,"ProdusDAO:findById "+ e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return returnez;
	}
	
	
	public static int insert(Produs produs) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;
		
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, produs.getId());
			insertStatement.setString(2, produs.getNume());
			insertStatement.setString(5, produs.getProducator());
			insertStatement.setString(3, produs.getDataFab());
			insertStatement.setString(4, produs.getDataExp());
			insertStatement.setInt(6, produs.getCant());
			insertStatement.setInt(7, produs.getPret());
			insertStatement.setString(8, produs.getUnitate());
			insertStatement.executeUpdate();
			
			ResultSet rs = insertStatement.getGeneratedKeys();
			if(rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProdusDAO insert "+ e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}

	public static Produs deleteProdus(int idProdus) {
		Produs returnez =null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		int rs = 0;
		
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setLong(1,idProdus);
			rs = deleteStatement.executeUpdate();
						
		}catch(SQLException e){
			LOGGER.log(Level.WARNING,"ProdusDAO delete "+ e.getMessage());
		}
		finally {
			
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		return returnez;
	}
	
	public static int update(int id,Produs produs) throws SQLException {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		PreparedStatement subStatement = null;
		
		int Id;
		String nume;
		String producator;
		String datafab,dataexp;
		int cant;
		int pret;
		String unitate;
		
		int insertedId = -1;
		
		try {

			insertStatement = dbConnection.prepareStatement(insertStatementString,Statement.RETURN_GENERATED_KEYS);
			ResultSet rs1= null;
			
			subStatement = dbConnection.prepareStatement(findStatementString1,Statement.RETURN_GENERATED_KEYS);
			subStatement.setLong(1, id);
			rs1 = subStatement.executeQuery();
			
			
			while(rs1.next()) {
				Id = rs1.getInt("idProdus");
				nume= rs1.getString("Nume");
				datafab= rs1.getString("DataFab");
				dataexp= rs1.getString("DataExp");
				producator= rs1.getString("Producator");
				cant = rs1.getInt("Cant");
				pret = rs1.getInt("Pret");
				unitate= rs1.getString("Unitate");
									
			Produs p=new Produs(Id,nume,datafab,dataexp,producator,cant,pret,unitate);
			ProdusBLL produsbll= new ProdusBLL();			
			produsbll.deleteProdusById(id);
			produsbll.insertProduse(produs);

			
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProdusDAO update "+ e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(subStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}

	
public static int update1(int id,Produs produs) throws SQLException {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		int rs1= 0;
		ProdusBLL produs1 = new ProdusBLL();
		Produs p=produs1.findProdusById(id);
		try {
			
			int Id=p.getId();
			String nume=p.getNume();
			String datafab= p.getDataFab();
			String dataexp= p.getDataExp();
			String producator= p.getProducator();
			int cant = p.getCant();
			int pret =p.getPret();
			String unitate = p.getUnitate();
						
			
			updateStatement = dbConnection.prepareStatement(updateStatementString,Statement.RETURN_GENERATED_KEYS);
			updateStatement.setInt(1, Id);				
			updateStatement.setString(2, nume);
			updateStatement.setString(3, datafab);
			updateStatement.setString(4, dataexp);
			updateStatement.setInt(6, cant);
			updateStatement.setInt(7, pret);
			updateStatement.setString(8, unitate);

			rs1 = updateStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO update "+ e.getMessage());
	}
		return rs1;
	}
}


