package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mysql.jdbc.Statement;

import Connection.ConnectionFactory;
import bll.BonuriBLL;
import bll.ProdusBLL;
import model.Bonuri;
import model.Produs;

public class BonuriDAO {

	protected static final Logger LOGGER = Logger.getLogger(ProdusDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO bonuri (idBonuri,idClient,idProdus,NumeC,NumeP,cantitate,pretfinal)"+"VALUES(?,?,?,?,?,?,?)";
	private final static String findStatementString= "Select * From bonuri ";	
	private final static String deleteStatementString= "Delete From bonuri where idBonuri = ?";
	private final static String findStatementString1= "Select * From bonuri where idBonuri = ?";
	private final static String updateStatementString ="Select * From produs where idProdus = ?";
	private final static String updateStatementString1 ="Select * From client where idClient =?";
	
	public static Bonuri findById(int idBonuri) {
		Bonuri returnez = null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, idBonuri);
			rs = findStatement.executeQuery();
			rs.next();
			
			int idBonuri1 = rs.getInt("idBonuri");
			int idClient = rs.getInt("idClient");
			int idProdus = rs.getInt("idProdus");
			String NumeC = rs.getString("NumeC");
			String NumeP = rs.getString("NumeP");
			int cant= rs.getInt("cantitate");
			int pf = rs.getInt("pretfinal");
			returnez = new Bonuri(idBonuri1,idClient,idProdus,NumeC,NumeP,cant,pf);
			
			
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING,"BonuriDAO:findById "+ e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return returnez;
	}
	
	public static ArrayList<Bonuri> findAll() {
		ArrayList<Bonuri> returnez = new ArrayList<Bonuri>();
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			
			rs = findStatement.executeQuery();
			while(rs.next()){
			
			int idBonuri = rs.getInt("idBonuri");
			int idClient = rs.getInt("idClient");
			int idProdus = rs.getInt("idProdus");
			String NumeC = rs.getString("NumeC");
			String NumeP = rs.getString("NumeP");
			int cant= rs.getInt("cantitate");
			int pf = rs.getInt("pretfinal");
			returnez.add(new Bonuri(idBonuri,idClient,idProdus,NumeC,NumeP,cant,pf));
			}
			
		} catch(SQLException e) {
			LOGGER.log(Level.WARNING,"BonuriDAO:findById "+ e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return returnez;
	}
	
	
	public static int insert(Bonuri bon) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		PreparedStatement subStatement =null;
		PreparedStatement subStatement1 =null;
		PreparedStatement findStatement = null;
		
		int insertedId = -1;
		String nume;
		int idClient;
		String NumeC;
		int IdProdus;
		
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs1=null;
			ResultSet rs2=null;
			
			subStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
			subStatement.setLong(1,bon.getIdProdus());
			subStatement1 = dbConnection.prepareStatement(updateStatementString1, Statement.RETURN_GENERATED_KEYS);
			subStatement1.setLong(1,bon.getIdClient());
			
			rs1 =  subStatement.executeQuery();
			rs2 = subStatement1.executeQuery();
			
			while(rs1.next() && rs2.next()) {
				
				IdProdus = rs1.getInt("idProdus");
				nume =rs1.getString("Nume");
				String producator=rs1.getString("Producator");
				String datafab = rs1.getString("DataFab");
				String dataexp = rs1.getString("DataExp");
				int cantProdus= rs1.getInt("cant");
				String unitate= rs1.getString("unitate");
				int pret = rs1.getInt("pret");	
				
				idClient = rs2.getInt("idClient");
				NumeC= rs2.getString("Nume");
			
			
		if(bon.getIdProdus()==IdProdus) {
			if(cantProdus >= bon.getCantitate()) {
					Produs PN= new Produs(IdProdus,nume,producator,datafab,dataexp,cantProdus-bon.getCantitate(),pret,unitate);
					ProdusBLL produsBll= new ProdusBLL();
					produsBll.deleteProdusById(IdProdus);
					produsBll.insertProduse(PN);
					
					insertStatement.setInt(1, bon.getIdBonuri());
					insertStatement.setInt(2, idClient);
					insertStatement.setInt(3, IdProdus);
					insertStatement.setString(4, NumeC);
					insertStatement.setString(5, nume);
					insertStatement.setInt(6, bon.getCantitate());
					insertStatement.setInt(7, bon.getPretfinal() + bon.getCantitate()*pret);
					insertStatement.executeUpdate();
					
					break;
				}else {
					throw new NoSuchElementException("There aren't enough resources");
				}
			}else {
				throw new NoSuchElementException("There aren't any product with this id ");
			}
		}	
			
			rs2.next();
			ResultSet rs = insertStatement.getGeneratedKeys();
			if(rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"BonuriDao insert "+ e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
			ConnectionFactory.close(subStatement);
			ConnectionFactory.close(subStatement1);
		}
		return insertedId;
	}

	public static Bonuri deleteProdus(int idBon) {
		Bonuri returnez =null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		int rs = 0;
		
		try{
			
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setLong(1,idBon);
			rs = deleteStatement.executeUpdate();
		
		}catch(SQLException e){
			LOGGER.log(Level.WARNING,"BonuriDAO delete "+ e.getMessage());
		}
		finally {
			
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		return returnez;
	}
	
	public static int update(int id,Bonuri bon) throws SQLException{
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement =null;
		PreparedStatement subStatement =null;
		
		int idb;
		int idc;
		int idp;
		String nume;
		String prod;
		int cant;
		int pf;
		
		int insertedId = -1;
		
		try {
				insertStatement =dbConnection.prepareStatement(insertStatementString,Statement.RETURN_GENERATED_KEYS);
				ResultSet rs1=null;
				
				subStatement = dbConnection.prepareStatement(findStatementString1,Statement.RETURN_GENERATED_KEYS);
				subStatement.setLong(1,id);
				rs1 = subStatement.executeQuery();
				
				while(rs1.next()) {
					idb = rs1.getInt("idBonuri");
					idc = rs1.getInt("idClient");
					idp = rs1.getInt("idProdus");
					nume = rs1.getString("NumeC");
					prod= rs1.getString("NumeP");
					cant = rs1.getInt("Cantitate");
					pf = rs1.getInt("PretFinal");
					
					BonuriBLL bonuriBll = new BonuriBLL();
					bonuriBll.deleteBonuriById(id);
					bonuriBll.insertBonuri(bon);
				}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO update "+ e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(subStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
}
